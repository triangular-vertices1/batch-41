import sys
def grid(height):
    constant = 0
    space = height + 1
    for i in range (height):
        for j in range(space):
            print(end = " ")
        space = space - 1
        for j in range (i + 1):
            constant = constant + 1
            print(constant, end = "  ")
        print()
def row_col(point):
    row = 1
    temp = 1
    while point >= temp + row:
        temp += row
        row += 1
    return row, point - temp
def triangle(allPoints):
    row = [row_col(point)[0] for point in allPoints]
    col = [row_col(point)[1] for point in allPoints]
    if row[0] == row[1]:
        len = col[1] - col[0]
        return len > 0 and col[2] == col[1] and row[2] == row[0] + len
    else:
        len = row[1] - row[0]
        return len > 0 and col[0] == col[1] and row[2] == row[1] and col[2] == col[1] + len
def parallelogram(allPoints):
    row = [row_col(point)[0] for point in allPoints]
    col = [row_col(point)[1] for point in allPoints]

    if row[0] == row[1]:
        len = col[1] - col[0]
        return len > 0 and row[2] == row[3] and col[3] - col[2] == len and row[2] - row[0] == len and (col[2] == col[0] or col[2] == col[1])
    else:
        len = row[1] - row[0]
        return len > 0 and col[0] == col[1] and col[2] == col[3] and row[1] == row[2] and row[3] - row[2] == len and col[2] - col[1] == len
def hexagon(allPoints):
    row = [row_col(point)[0] for point in allPoints]
    col = [row_col(point)[1] for point in allPoints]

    len = col[1] - col[0]

    return len > 0 and row[0] == row[1] and col[2] == col[0] and row[2] == row[0] + len and col[3] == col[1] + len and row[3] == row[2] and col[4] == col[1] and row[4] == row[2] + len and col[5] == col[3] and row[5] == row[4]
allPoints = [sys.argv[i] for i in range(1,len(sys.argv))]
allPoints =  list(map(int,allPoints))
allPoints.sort()
height = len(allPoints)

is_valid = ((height == 3) and triangle(allPoints)) or ((height == 4) and parallelogram(allPoints)) or ((height == 6) and hexagon(allPoints))
if is_valid :
    if height == 3:
        print(allPoints, "are the vertices of a triangle")
    elif height == 4:
        print(allPoints, "are the vertices of a parallelogram")
    elif height == 6:
        print(allPoints, "are vertices of a hexagon")
else:
    print(allPoints, "are not the vertices of an acceptable figure")
grid(height)
