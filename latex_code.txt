\documentclass{beamer}

\mode<presentation>
{
  \usetheme{Darmstadt}
  \usecolortheme{seahorse}
  \usefonttheme{structurebold}
  \usefonttheme{structurebold}
  \setbeamertemplate{navigation symbols}{}
  \setbeamertemplate{caption}[numbered]
}

\usepackage[utf8]{inputenc}
\usepackage[english]{babel}
\usepackage[T1]{fontenc}
\usepackage{graphicx}

\title{TRIANGULAR VERTICES}

\subtitle{Batch-41}
\author{\textbf{DIKKALA NAVYA-21B01A1246-IT (LEAD)\\
V.VENKATA SRUTHI-21B01A54B5-AIDS\\
V.LAKSHMI VIJAYA-21B01A54B4-AIDS\\
RAMAYANAM ABHIGNA-21B01A12F3-IT\\
K.PUJITHA-21B01A0220-EEE}}
\institute{\Large SVECW}
\date{March 25,2023}
\begin{document}
   \begin{frame}
     \titlepage
   \end{frame}

\section{python}
\begin{frame}{\Large INTRODUCTION ABOUT THE PROJECT:}
\begin{itemize}
    \item\textbf{The problem statement describes about an infinite triangular grid which takes a set of points as an input and predicts the shape of the given vertices.}\\\
\item\textbf{This is a Python program that takes in command line arguments representing the vertices of a 2D figure, and checks if they form a valid structure.}\\\
\item\textbf{If the vertices satisfies the given conditions it prints respective structure else print not acceptable.}
\end{itemize}
\end{frame}
\begin{frame}{\Large Triangular-Grid:}
\begin{figure}[htp]
      \centering
      \includegraphics[width=7cm]{WhatsApp Image 2023-03-24 at 3.55.19 PM.jpeg}
      \caption{Reference Grid}
      \label{fig:code}
  \end{figure}
\end{frame}
\begin{frame}{\Large APPROACH:}
\begin{itemize}
\item \textbf {We have read the problem statement carefully and define a triangular grid function.}\\\
\item \textbf {Create different functions to predict the shape of the vertices i.e (Triangle,Parallelogram,Hexagon).}\\\
\item \textbf{We studied the properties of the given statement.\\\
1.Each side of figure must coincide with edge of grid.  \\
2.All sides of figure must be of the same length.}
\end{itemize}    
\end{frame}
\begin{frame}{\Large LEARNINGS:}
\begin{itemize}
\item \textbf {We have improved our problem solving skills through intellectual.}\\\
\item \textbf{We demonstrate knowledge of software engineering practices.}\\\
\item \textbf {We have developed our communication ability}\\\
\item \textbf {we improved our cooperative team work.}\\
\end{itemize}
    
\end{frame}
\begin{frame}{\Large CHALLENGES:}
   
 \begin{itemize}
    \item\textbf{We were challenged to run the code without errors.}\\\
    \item \textbf {We were challenged to determine the geometrical shape(Hexagon).}\\\
    \item \textbf {We overcame through the two conditions give in the problem statement.}\\\
   
    \end{itemize}
   
\end{frame}

\begin{frame}{\Large STATISTICS:}
    \begin{itemize}
        \item \textbf{Number of Lines of Code: 58}\\\
        \item \textbf{The code has 5 user-defined functions i.e(Grid,Row-Col,Triangle,Parallelogram,Hexagon).}
    \end{itemize}
    
\end{frame}

\begin{frame}{DEMO/SCREENSHOTS:}
  \begin{figure}[htp]
      \centering
      \includegraphics[width=10cm]{Screenshot (19).png}
      \caption{Code for the project}
      \label{fig:code}
  \end{figure}
  
\end{frame}
\begin{frame}{DEMO/SCREENSHOTS:}
  \begin{figure}[htp]
      \centering
      \includegraphics[width=10cm]{Screenshot (20).png}
      \caption{Code for the project}
      \label{fig:code}
  \end{figure}
  
\end{frame}
\begin{frame}
\centering
\textbf{\Huge THANK YOU}
    
\end{frame}

\end{document}